package es.upm.dit.apsv.cris.model;

import java.io.Serializable;

import javax.persistence.Entity;

import javax.persistence.Id;

@Entity
public class Publication implements Serializable {
	private static final long serialVersionUID =1L;
	
	@Id
	private String id;
	private String title;
	private String PublicationName;
	private String publicationDate;
	private String authors;
	private int citeCount;
	public Publication() {
		super();
		
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPublicationName() {
		return PublicationName;
	}
	public void setPublicationName(String publicationName) {
		PublicationName = publicationName;
	}
	public String getPublicationDate() {
		return publicationDate;
	}
	public void setPublicationDate(String publicationDate) {
		this.publicationDate = publicationDate;
	}
	public String getAuthors() {
		return authors;
	}
	public void setAuthors(String authors) {
		this.authors = authors;
	}
	public int getCiteCount() {
		return citeCount;
	}
	public void setCiteCount(int citeCount) {
		this.citeCount = citeCount;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((PublicationName == null) ? 0 : PublicationName.hashCode());
		result = prime * result + ((authors == null) ? 0 : authors.hashCode());
		result = prime * result + citeCount;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((publicationDate == null) ? 0 : publicationDate.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Publication other = (Publication) obj;
		if (PublicationName == null) {
			if (other.PublicationName != null)
				return false;
		} else if (!PublicationName.equals(other.PublicationName))
			return false;
		if (authors == null) {
			if (other.authors != null)
				return false;
		} else if (!authors.equals(other.authors))
			return false;
		if (citeCount != other.citeCount)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (publicationDate == null) {
			if (other.publicationDate != null)
				return false;
		} else if (!publicationDate.equals(other.publicationDate))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Publication [id=" + id + ", title=" + title + ", PublicationName=" + PublicationName
				+ ", publicationDate=" + publicationDate + ", authors=" + authors + ", citeCount=" + citeCount + "]";
	}
}